package sortersTesterClasses;

import sorterClasses.BubbleSortSorter;

public class Main {
	public static void main(String[] args) {
		// Create array of Entero
		Entero[] arr = new Entero[5];
		arr[0] = new Entero(5);
		arr[1] = new Entero(9);
		arr[2] = new Entero(2);
		arr[3] = new Entero(6);
		arr[4] = new Entero(8);
		tester(arr);
		Integer[] arr_int = new Integer[]{5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10};
		tester(arr_int);
	}
	public static void tester(Entero[] enteros) {
		BubbleSortSorter<Entero> sortero = new BubbleSortSorter<Entero>();
		
		// Print for good measure
		System.out.println("Before sorting Entero[] with default comparator: ");
		showArray(enteros);
		sortero.sort(enteros, null);
		System.out.println("After sorting: ");
		showArray(enteros);
		System.out.println("******************************************************************");
	}
	
	public static void tester(Integer[] integers) {
		BubbleSortSorter<Integer> sortero = new BubbleSortSorter<Integer>();
		
		// Print for good measure
		System.out.println("Before sorting Integer[] in ascending order with Bubble Sort comparator: ");
		showArray(integers);
		sortero.sort(integers, new IntegerComparator1());
		System.out.println("After sorting in ascending order: ");
		showArray(integers);
		sortero.sort(integers, new IntegerComparator2());
		System.out.println("After sorting in descending order: ");
		showArray(integers);	
	}
	
	private static void showArray(Object[] a) {
		for (int i=0; i<a.length; i++) 
			System.out.print("\t" + a[i]); 
		System.out.println();
	}
}
